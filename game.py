from typing import List, Tuple, Set
from game_board import OmokBoard
from game_rule import RenjuRule


class OmokGame:
    def __init__(self):
        self.board: OmokBoard = OmokBoard()
        self.rule: RenjuRule = RenjuRule(self.board)
        # 1 for black, 2 for white
        self.stones: List[int] = [1, 2]

    def reset_game(self) -> None:
        # maybe just use something like self.board.__init__()?
        self.board.reset_board()

    def make_move(self, coord: Tuple[int, int], forbidden_moves) -> bool:
        move = self.board.coord_to_move(coord)
        # set subtraction to get legal moves for black
        # self.legal_moves: Set[int] = self.board.available_moves - forbidden_moves
        self.legal_moves: Set[int] = self.get_legal_moves(forbidden_moves)
        if not (self.rule.is_invalid(coord[1], coord[0]) or not move in self.legal_moves):
            self.board.place_stone(coord, self.current_stone)
            return True
        return False

    def get_legal_moves(self, forbidden_moves) -> Set[int]:
        return self.board.available_moves - forbidden_moves
    
    def start_game(self, start_player: int=0) -> None:
        '''
        start_player -> 0 for human, 1 for AI
        '''
        self.current_stone = self.stones[0]
        # black starts first at the centre by the rule
        coord = (self.board.height // 2, self.board.width // 2)
        self.board.place_stone(coord, self.current_stone)
        print(self.board.table)
        self.current_stone = self.stones[1]

        while True:
            forbidden_moves = set()
            if self.current_stone == 1:
                forbidden_moves = self.rule.get_forbidden_points(self.current_stone)
                print(forbidden_moves)
            coord = tuple(map(int, input('Coordinate: ').split()))
            if self.make_move(coord, forbidden_moves):
                index = (self.stones.index(self.current_stone) + 1) % 2
                # Change this as a function and return rewards?
                if self.rule.is_gameover(coord[1], coord[0], self.current_stone) or len(self.board.available_moves) == 0:
                    print(f'Player {self.current_stone} wins')
                    print(self.board.table)
                    break
                self.current_stone = self.stones[index]
            else:
                print('Please choose appropriate coordinate: ')

            # Refactor above if branches using inversion?
            # check_move = self.make_move(coord, forbidden_moves)
            # if not check_move: 
            #     print('Please choose appropriate coordinate: ')
            #     continue

            print(self.board.table)


if __name__ == "__main__":
    game = OmokGame()
    game.start_game()
