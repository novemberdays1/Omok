from typing import Set, Tuple
import numpy as np


class OmokBoard:
    def __init__(self, width: int=15, height: int=15):
        self.width: int = width
        self.height: int = height
        # self.moves_board = np.arange(self.width * self.height).reshape(self.width, self.height)
        self.reset_board()

    def reset_board(self) -> None:
        self.available_moves: Set[int] = set(range(self.width * self.height))
        self.last_move = -1 # indicates new game
        self.table = np.zeros((self.width, self.height))

    def coord_to_move(self, coord: Tuple[int, int]) -> int:
        '''
        Moves in 3 x 3 board; move 5 -> (row, col) = (1, 2)
        0 1 2
        3 4 5
        6 7 8
        '''
        return coord[0] * self.width + coord[1]

    def move_to_coord(self, move) -> Tuple[int, int]:
        row = move // self.height
        width = move % self.width
        return (row, width)

    def place_stone(self, coord: Tuple[int, int], stone: int) -> None:
        self.table[coord] = stone
        self.available_moves.remove(self.coord_to_move(coord))
        self.last_move = self.coord_to_move(coord)


if __name__ == "__main__":
    b = OmokBoard()
    for y in range(15):
        for x in range(15):
            b.table[(y, x)] = b.coord_to_move((y, x))
    print(b.table)
