# Omok
Implementation of Gomoku(Omok) board game in Python with agent.

Gomoku is a board game wildely played in North East Asian nations (China, Korea, and Japan); it is played with Go pieces on 15x15 Go board (19x19 for Go). To win Gomoku, a player has to connect stone five in a row. However, due to its deterministic nature, first player has a higher chance of winning the game, hence professional Gomoku uses Soosõrv Opening with Renju rule to balance up the winning chance.

This project will implement Gomoku (Renju rule wo/ Soosõrv) and its AI agent using techniques used to train AlphaZero with Python. Potentially, once the above is done, it might be further implemented with PyGame and other agents.

Plan:
- Implementing bare bone Gomoku
- Add Renju rule
- Add Monte Carlo tree search
- Implement AlphaZero algorithm
- Extensions: PyGame, multithreaded MCTS, other agents.

Sources:

For implementing Renju rule, the majority of code came from [this](https://m.blog.naver.com/PostView.naver?isHttpsRedirect=true&blogId=dnpc7848&logNo=221506783416) with some minor modifications.
