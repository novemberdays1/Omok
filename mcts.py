from collections import deque
from typing import List, Set
import math
import random
import numpy as np


class Node:
    def __init__(self, game, move, parent=None, action_took=None, **kwargs):
        self.game = game
        self.move = move
        self.parent = parent
        self.action_took = action_took
        self.children: List[Node] = []
        # Set is passed by reference, might need to change it?
        # self.expandable_moves: Set[int] = self.game.legal_moves
        self.expandable_moves: List[int] = list(self.game.legal_moves)

        self.c = kwargs['c']
        self.ucb = 0
        self.num_visited, self.value_sum = 0, 0


    # def is_fully_expanded(self):
    #     return not self.expandable_moves and self.children
    def is_expanded(self):
        # empty set returns false
        # return self.expandable_moves and (len(self.expandable_moves) != len(self.children))
        # add logic when node expanded, corresponding expandable move removed
        # also need to consider when one of expandable_moves is terminal state?
        # unlikely going to reach terminal state btw
        return not self.expandable_moves

    def compute_ucb(self, node):
        '''
        Q in [-1, 1] -> [0, 1]
        Child and parent are different players, so Q should be complemented for each depth of the tree
        '''
        q_value = 1 - ((node.value_sum / node.visit_count) + 1) / 2
        return q_value + self.c * math.sqrt(math.log(self.num_visited) / node.visit_count)
    
    def select(self):
        '''
        Selecting best child based on the UCB score of children nodes. This is only used when there is no more expandable moves available for the root/parent node.
        '''
        max_ucb = -float('inf')
        best_child = None

        for child in self.children:
            ucb = self.compute_ucb(child)
            if ucb > max_ucb:
                max_ucb = ucb
                best_child = child
        return best_child

    def expand(self):
        random_move = random.choice(self.expandable_moves)

class MCTS:
    def __init__(self):
        pass
